-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2019 at 06:08 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `barbershop`
--
CREATE DATABASE IF NOT EXISTS `barbershop` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `barbershop`;

-- --------------------------------------------------------

--
-- Table structure for table `detailpembayaran`
--

CREATE TABLE `detailpembayaran` (
  `nofaktur` varchar(11) NOT NULL,
  `kdpelayanan` varchar(6) NOT NULL,
  `harga` decimal(10,0) NOT NULL,
  `qty` int(11) NOT NULL,
  `jumlah` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `headerpembayaran`
--

CREATE TABLE `headerpembayaran` (
  `nofaktur` varchar(11) NOT NULL,
  `tanggal` date NOT NULL,
  `namapelanggan` varchar(50) NOT NULL,
  `idpegawai` varchar(6) NOT NULL,
  `bayar` decimal(10,0) NOT NULL,
  `sisa` decimal(10,0) NOT NULL,
  `total` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `idpegawai` varchar(6) NOT NULL,
  `namapegawai` varchar(50) NOT NULL,
  `jeniskelamin` varchar(20) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `alamatpegawai` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`idpegawai`, `namapegawai`, `jeniskelamin`, `telp`, `alamatpegawai`) VALUES
('PG002', 'Hindayanti', 'P', '0877 8191 9018', 'Jl. Mangga Manis Dua'),
('PG003', 'Yanaf', 'L', '08675322346', 'Tanggerang Selatan'),
('PG004', 'Yusuf', 'L', '0877 8190 7013', 'Jl. Nangka No.8A');

-- --------------------------------------------------------

--
-- Table structure for table `pelayanan`
--

CREATE TABLE `pelayanan` (
  `kdpelayanan` varchar(6) NOT NULL,
  `namapelayanan` varchar(50) NOT NULL,
  `hargapelayanan` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelayanan`
--

INSERT INTO `pelayanan` (`kdpelayanan`, `namapelayanan`, `hargapelayanan`) VALUES
('kd001', 'potong rambut', '15000');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `iduser` varchar(6) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`iduser`, `username`, `password`) VALUES
('1', 'user1', '24c9e15e52afc47c225b757e7bee1f9d');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detailpembayaran`
--
ALTER TABLE `detailpembayaran`
  ADD PRIMARY KEY (`nofaktur`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`idpegawai`);

--
-- Indexes for table `pelayanan`
--
ALTER TABLE `pelayanan`
  ADD PRIMARY KEY (`kdpelayanan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
