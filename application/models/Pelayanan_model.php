<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai_model extends CI_Model
{ 
	//panggil nama tabel
	private $_table = "pelayanan";

	public function rules(){
		return
		[
					[ 
						'field'=> 'kdpelayanan',
						'label' => 'kdpelayanan',
						'rules' => 'required|max_length[6]',
						'errors' => [
							'requered' => 'kdpelayanan tidak boleh kosong.',
							'max_length' => 'kdpelayanan tidak boleh lebih dari 6 karakter.',
						]
					],
					[
						'field' => 'namapelayanan',
						'label'  => 'Nama pelayanan',
						'rules' => 'required',
						'errors' =>[
							'required' => 'Nama pelayanan tidak Boleh kosong.',
						]
					],
					[
						'field' => 'hargapelayanan',
						'label'  => 'hargapelayanan',
						'rules' => 'required',
						'errors' =>[
							'required' => 'hargapelayanan tidak Boleh kosong.',
							
						]
					]
					
		];
	}


	public function tampilDataPelayanan()
		{
			//seperti : select * from <nama_table>
			return $this->db->get($this->_table)->result();
		}

	public function tampilDataPelayanan2()
		{
			//KETIKA MAKE QUERY
			$query = $this->db->query("SELECT * FROM pelayanan WHERE flag = 1");
			return $query->result();
		}

	public function tampilDataPelayanan3()
		{
			//MAKE QUERY BUILDER
			$this->db->select('*');
			$this->db->order_by('kdpelayanan', 'ASC');
			$result = $this->db->get($this->_table);
			return $result->result();
		}
	public function save()
		{
			
			$kdpelayanan	= $this->input->post('kdpelayanan');
			$sql = $this->db->query("SELECT kdpelayanan FROM pelayanan where kdpelayanan='$kdpelayanan'");
			$cek_kd = $sql->num_rows();
				if($cek_kd>0)
			{
				$this->session->set_flashdata();
				redirect('Pelayanan/inputpelayanan');
			}else{
				$kdpelayanan	= $this->input->post('kdpelayanan');
			}
			// $tgl_gabung 	= $tahun . "_" . $bulan .  "_" . $tgl;

			$data['kdpelayanan'] 			=$kdpelayanan;
			$data['namapelayanan'] 		=$this->input->post('namapelayanan');
			$data['hargapelayanan'] 		=$this->input->post('hargapelayanan');
			
			$this->db->insert($this->_table, $data);
			//catetan $data['nama_lengkap']<-(seseuai database) =$this->input->post('nama_karyawan')<-seseuai inputkaryawan;

		}
	public function detail($kdpelayanan)
	{
		$this->db->select('*');
		$this->db->where('kdpelayanan', $kdpelayanan);
		$result = $this->db->get($this->_table);
		return $result->result();
	}


public function update($idpegawai)
	{
		
		$data['namapegawai']			= $this->input->post('namapegawai');
		$data['jeniskelamin']			= $this->input->post('jeniskelamin');
		$data['alamatpegawai']			= $this->input->post('alamatpegawai');
		$data['telp'] 				    = $this->input->post('telp');
		

		
		
		$this->db->where('idpegawai', $idpegawai);
		$this->db->update($this->_table, $data);
	}

	public function delete($idpegawai)
	
		{
			$this->db->where('idpegawai',$idpegawai);
			$this->db->delete($this->_table);
		}



	public function tampilDataPegawaiPagination($perpage, $uri, $data_pencarian)
	{
		// echo "<pre>";
		// print_r($data_pencarian); die();
		// echo "</pre>";
		$this->db->select('*');
		if (!empty($data_pencarian)) {
			$this->db->like('namapegawai', $data_pencarian);
		}
		$this->db->order_by('idpegawai','asc');

		$get_data = $this->db->get($this->_table, $perpage, $uri);
		if ($get_data->num_rows() > 0) {
			return$get_data->result();
		}else{
			return null;
		}

	}

	public function tombolpagination($data_pencarian)
	{
		// echo $data_pencarian;die;
		//echo "<pre>";
		//print_r($data_pencarian); die();
		//echo "</pre>";
		//cari jmlh data berdasarkan data pencarian
		$this->db->like('namapegawai', $data_pencarian);
		$this->db->from($this->_table);
		$hasil = $this->db->count_all_results();
		//echo "<pre>";
		//print_r($hasil); die();
		//echo "</pre>";

		//pagination limit
		$pagination['base_url']		= base_url().'Pegawai/listpegawai/load/';
		$pagination['total_rows']	=$hasil;
		$pagination['per_page']		= "3";
		$pagination['uri_segment']	= 4;
		$pagination['num_links']	= 2;

		//custom pagging configuration
		$pagination['full_tag_open']	= '<div class="pagination">';
		$pagination['full_tag_close']	= '</div>';

		$pagination['first_link']		= 'First Page';
		$pagination['first_tag_open']	= '<span class="firstlink">';
		$pagination['first_tag_close']	= '</span>';

		$pagination['last_link']		= 'Last Page';
		$pagination['last_tag_open']	= '<span class="lastlink">';
		$pagination['last_tag_close']	= '</span>';

		$pagination['next_link']		= 'Next Page';
		$pagination['next_tag_open']	= '<span class="nextlink">';
		$pagination['next_tag_close']	= '</span>';

		$pagination['prev_link']		= 'Prev Page';
		$pagination['prev_tag_open']	= '<span class="prevlink">';
		$pagination['prev_tag_close']	= '</span>';

		$pagination['cur_tag_open']		= '<span class="curlink">';
		$pagination['cur_tag_close']	= '</span>';

		$pagination['num_tag_open']		= '<span class="numlink">';
		$pagination['num_tag_close']	= '</span>';

		$this->pagination->initialize($pagination);

		$hasil_pagination	= $this->tampilDataPegawaiPagination($pagination['per_page'], $this->uri->segment(4), $data_pencarian);

		return $hasil_pagination;
	}
	public function createKodeUrut(){
	//cek kode barang terakhir
	$this->db->select('MAX(idpegawai) as idpegawai');
	$query  = $this->db->get($this->_table);
	$result = $query->row_array(); //hasil bentuk array

	$id_terakhir = $result['idpegawai'];
	//format BR001 = BR (label awal), 001 (nomor urut)
	$label = "PG";
	$no_urut_lama = (int) substr($id_terakhir, 2,3);
	$no_urut_lama ++;

	$no_urut_baru = sprintf("%03s", $no_urut_lama);
	$id_baru = $label . $no_urut_baru;

	return $id_baru;
}
}
