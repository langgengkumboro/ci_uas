<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
	//panggil nama table
	private $_table = "user";
	
	public function cekUser($username, $password)
	{
		$this->db->select('username');
		$this->db->where('username', $username);
		$this->db->where('password',md5($password));

		$result = $this->db->get($this->_table);
		return $result->row_array();
	}
}
