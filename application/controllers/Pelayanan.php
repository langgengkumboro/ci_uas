<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelayanan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait (manggil pertama kali scrip di jalanin)
		$this->load->model("Pelayanan_model");
		// $this->load->model("Jabatan_models");


		//load validasi
		$this->load->library('form_validation');

		//cek sesi login
		//$user_login	= $this->session->userdata();
		//if()
		//load validasi
	
		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	}

	public function index()
	{
		$this->listPelayanan();


	}
	public function listPelayanan()
	{
		//$data['data_Karyawan'] = $this->Karyawan_model->tampilDataKaryawan();
		//$data['content']	='forms/list_karyawan';
		//$this->load->view('Home_2', $data);


		if (isset($_POST['cari_data']) != null) {
			$data['kata_pencarian'] = $this->input->post('cari_nama');
			$this->session->set_userdata('session_pencarianpelayanan', $data['kata_pencarian']);
		}else{
			$data['kata_pencarian'] =$this->session->userdata('session_pencarianpelayanan');
		}
		// 	echo "<pre>";
		// print_r($this->session->userdata()); die();
		// echo "</pre>";

		$data['data_Pelayanan']	= $this->Pegawai_model->tombolpagination($data['kata_pencarian']);

		$data['content']	= 'forms/list_pelayanan';
		$this->load->view('Home_2', $data);
	}


	public function inputpelayanan()
	{
		$data['kdpelayanan_baru'] = $this->Pelayanan_model->createKodeUrut();

		//if (!empty($_REQUEST)){
		//	$m_karyawan = $this->Karyawan_model;
		//	$m_karyawan->save();
		//	redirect("Karyawan/index", "refresh");
		$validation = $this->form_validation;
		$validation->set_rules($this->Pelayanan_model->rules());

		if ($validation->run()){
			$this->Pelayanan_model->save();
			$this->session->set_flashdata('info', '<div style="color : green">SIMPAN DATA BERHASIL HOREEE </div>');
			redirect("Pelayanan/index", "refresh");
			}
			$data['content']       ='forms/inputpelayanan';
			$this->load->view('home_2', $data);
		
	}
	public function editpelayanan($kdpelayanan)
	{	
		$data['detail_pelayanan']	= $this->Pelayanan_model->detailpelayanan($kdpelayanan);
		
		//if (!empty($_REQUEST)) {
		//		$m_karyawan = $this->karyawan_model;
		//		$m_karyawan->update($nik);
		//		redirect("karyawan/index", "refresh");
		$validation = $this->form_validation;
		$validation->set_rules($this->Pelayanan_model->rules());

		if ($validation->run()){
			$this->Pelayanan_model->update($kdpelayanan);
			$this->session->set_flashdata('info', '<div style="color : green">UPDATE DATA BERHASIL HOREEE </div>');
			redirect("Pelayanan/index", "refresh");
			}	
			
		
			$data['content']       ='forms/editpelayanan';
			$this->load->view('home_2', $data);	
	}

	public function delete($kdpelayanan)
	{
		$m_pelayanan = $this->Pelayanan_model;
		$m_pelayanan->delete($kdpelayanan);	
		redirect("Pelayanan/index", "refresh");	
	}
}
